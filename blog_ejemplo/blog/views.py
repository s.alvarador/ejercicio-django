from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Post
from django.utils import timezone
from django.urls import reverse
from .forms import PostForm
from django.views.generic import DetailView, ListView
# Create your views here.


class PostListView(ListView):
    model = Post
    template_name = "blog/index.html"


def post_lista(request):
    posts = None
    if request.user.is_authenticated:
        posts = Post.objects.all().order_by('fecha_publicacion')
    else:
        posts = Post.objects.filter(
            fecha_publicacion__lte=timezone.now()).order_by('fecha_publicacion')
    error = None
    if 'error' in request.session:
        error = request.session['error']
        del request.session['error']

    return render(request, 'blog/index.html', {'posts': posts, 'error': error})


def post_crear(request):
    if not request.user.is_authenticated:
        request.session['error'] = "Sin autorización para agregar nuevos post"
        return render(request, 'gestion_usuarios/login.html', {})

    if request.method == 'GET':
        post_form = PostForm(initial={'autor': request.user.pk})
        return render(request, 'blog/post_crear.html', {'post_form': post_form})
    elif request.method == 'POST':
        message = ""
        try:
            post_form = PostForm(data=request.POST)
            if post_form.is_valid():
                # post.fecha_publicacion = lambda fecha_publicacion : None if fecha_publicacion == "" else request.POST.get('fecha_publicacion')
                post = post_form.save()
                file = request.FILES['imagen']
                ext = file.name.split(".")[-1]
                file.name = str(post.pk) + "." + ext
                post.imagen = file
                post.save()
                message = "Éxito al guardar la publicación"
                return HttpResponseRedirect(reverse('blog:detalle', args=(post.pk,)))
            else:
                message = post_form.errors
                return render(request, 'blog/post_crear.html', {'message': message, 'post_form': post_form})
        except Exception as e:
            message = "Error al guardar la publicación: " + str(e)
            return render(request, 'blog/post_crear.html', {'message': message})
    else:
        response = HttpResponse('Método no permitido')
        response.status_code = 405
        return response


class PostDetailView(DetailView):
    model = Post
    template_name = "blog/post_detalle.html"

    #def get_context_data(self, **kwargs):
    #    context = super(PostDetailView, self).get_context_data(**kwargs)
    #    context['form'] = PostForm
    #    return context
#
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = super(PostDetailView, self).get_context_data(**kwargs)
        print(self.object.pk)
        post = get_object_or_404(Post, pk=self.object.pk)
        post.publish()
        return self.render_to_response( context=context)
    


def post_detalle(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        post.publish()
    try:
        return render(request, 'blog/post_detalle.html', {'post': post})
    except Exception as e:
        response = HttpResponse("Error al guardar la publicación: " + str(e))
        response.status_code = 404
        return response
