from django.db import models
from django.utils import timezone
from fcm_django.models import FCMDevice

# Create your models here.

class Post(models.Model):
    autor = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    titulo = models.CharField(max_length=200)
    contenido = models.TextField()
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_publicacion = models.DateTimeField(blank=True, null=True)
    imagen = models.FileField(blank=True, null=True, upload_to="blog")


    def publish(self):
        self.fecha_publicacion = timezone.now()
        devices = FCMDevice.objects.all()
        devices.send_message(title="Prueba", body="Cuerpo", data={"test": "test"})
        self.save()

    def __str__(self):
        return self.titulo
