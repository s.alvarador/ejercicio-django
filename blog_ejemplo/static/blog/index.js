$(function () {
  url_post = '/api/posts/';

  $.get(url_post).done(function (data) {
    $('#lista_posts').html('')
    data.results.forEach(function (item) {
      let titulo = item.titulo ? item.titulo : item.autor;
      let articulo = `<article class="col-12 col-md-4 col-lg-3">
            <article class="card">
              <img src="${item.imagen}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">
                  <a href="${item.url_local}">
                    ${titulo}
                  </a>
                </h5>
                <p class="card-text">
                ${item.contenido}
                </p>
              </div>
              <div class="card-footer text-muted">
                Publicado: ${(item.fecha_publicacion || '--')}
              </div>
            </article>
          </article>`;

      $('#lista_posts').append($.parseHTML(articulo))
    });
  })
})