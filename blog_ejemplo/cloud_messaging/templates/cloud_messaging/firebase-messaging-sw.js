// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.1.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.1.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object

// Initialize Firebase

firebase.initializeApp({
  apiKey: 'AIzaSyBKJwJ2qarpPq71DRiMYoUezMM7lk6AAPQ',
  authDomain: 'push-notifications-duoc.firebaseapp.com',
  databaseURL: 'https://push-notifications-duoc.firebaseio.com',
  projectId: 'push-notifications-duoc',
  storageBucket: 'push-notifications-duoc.appspot.com',
  messagingSenderId: '1026231640085',
  appId: '1:1026231640085:web:97f568bb1f0c5f4bc46e1c',
  measurementId: 'G-measurement-id',
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log('in service worker - in setBackgroundMessageHandler')
    const title = "Hello World";
    const options = {
        body: payload.data.status
    }
    return self.registration.showNotification(title, options);
});