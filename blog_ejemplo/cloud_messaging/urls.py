from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from . import views


urlpatterns = [
    path('firebase-messaging-sw.js', views.ServiceWorkerView.as_view(), name='service_worker')
]