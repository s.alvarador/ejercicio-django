from django.contrib.auth.models import User
from rest_framework import viewsets
from .serializers import UserSerializer, PostSerializer, PerfilUsuarioSerializer
from blog.models import Post
from gestion_usuarios.models import PefilUsuario

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class PefilUsuarioViewSet(viewsets.ModelViewSet):
    queryset = PefilUsuario.objects.all()
    serializer_class = PerfilUsuarioSerializer

class PostViewSet(viewsets.ModelViewSet):
    #queryset = Post.objects.filter(fecha_publicacion__isnull=False)
    queryset = Post.objects.all()
    serializer_class = PostSerializer