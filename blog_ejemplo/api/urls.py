from django.urls import include, path
from rest_framework import routers
from . import views
from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet
from rest_framework.authtoken import views as views_token

router = routers.DefaultRouter()
router.register('usuarios', views.UserViewSet)
router.register('perfil_usuario', views.PefilUsuarioViewSet)
router.register('posts', views.PostViewSet)

router.register('devices', FCMDeviceAuthorizedViewSet)


'localhost:8000/api/'
'localhost:8000/api/usuarios'
'localhost:8000/api/perfil_usuario'
'localhost:8000/api/posts'

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    
    path('api-token-auth/', views_token.obtain_auth_token)
]
    