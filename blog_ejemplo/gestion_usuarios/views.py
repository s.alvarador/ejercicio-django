from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from .forms import RegistrarForm, PefilUsuarioForm
from rest_framework.authtoken.models import Token

# Para iniciar sesión

def usuario_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('blog:index'))


def usuario_login(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('blog:index'))
    else:
        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    token, created = Token.objects.get_or_create(user=user)
                    reponse = HttpResponseRedirect(reverse('blog:index'))
                    reponse.set_cookie('token', str(token))
                    return reponse
                else:
                    return HttpResponse("Tu cuenta está inactiva.")
            else:
                print("username: {} - password: {}".format(username, password))
                return HttpResponse("Datos inválidos")
        else:
            return render(request, 'gestion_usuarios/login.html', {})

def registrar(request):
    registrado = False
    if request.method == 'POST':
        user_form = RegistrarForm(data=request.POST)
        profile_form = PefilUsuarioForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            if 'foto_perfil' in request.FILES:
                profile.foto_perfil = request.FILES['foto_perfil']
            profile.save()
            registrado = True
        else:
            print(user_form.errors, profile_form.errors)
    else:
        user_form = RegistrarForm()
        profile_form = PefilUsuarioForm()

    return render(request, 'gestion_usuarios/registrar.html',
                  {'user_form': user_form,
                   'profile_form': profile_form,
                   'registrado': registrado})
